
docker login --username fuensalida -p 7bcbe82c-b851-4d28-99ab-57e4b031a1e8
docker tag bb38976d03c fuensalida/enanoverde:tagname
docker push fuensalida/enanoverde:tagname



RUN mkdir /backend
WORKDIR /backend
COPY requirements.txt /backend/
COPY app.py /backend/
RUN pip install -r requirements.txt
COPY . /backend/


docker tag local-image:tagname new-repo:tagname
docker push new-repo:tagname

docker-miniconda
Docker container with a bootstrapped installation of Miniconda (based on Python 3.5) that is ready to use.

The Miniconda distribution is installed into the /opt/conda folder and ensures that the default user has the conda command in their path.

Anaconda is the leading open data science platform powered by Python. The open source version of Anaconda is a high performance distribution and includes over 100 of the most popular Python packages for data science. Additionally, it provides access to over 720 Python and R packages that can easily be installed using the conda dependency and environment manager, which is included in Anaconda.

Usage
You can download and run this image using the following commands:

```
docker pull continuumio/miniconda3
docker run -i -t continuumio/miniconda3 /bin/bash
Alternatively, you can start a Jupyter Notebook server and interact with Miniconda via your browser:

docker run -i -t -p 8888:8888 continuumio/miniconda3 /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet && mkdir /opt/notebooks && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser"
You can then view the Jupyter Notebook by opening http://localhost:8888 in your browser, or http://<DOCKER-MACHINE-IP>:8888 if you are using a Docker Machine VM.
```

    https://gitlab.com/fuensalida.carlos/shellghost.git
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker tag "$CI_REGISTRY_IMAGE" fuensalida/enanoverde:alfa
    - docker push "$CI_REGISTRY_IMAGE"
    - docker logout
    - docker login -u fuensalida -p 7bcbe82c-b851-4d28-99ab-57e4b031a1e8
    - docker push fuensalida/enanoverde:alfa
    - docker logout

Folder Structure:
src
  ____ app.py
  ____ templates
        ____ index.html
  ____ requirements.txt
  ____ Dockerfile
