"""
                                        :==+==+
                                      :+ooooooo=:
                                    :=ooooooooooo=+
                                  :=ooooooooooooooo+:
                                .=ooooooooooooo+ooooo+:
                              :=ooooooooooooooooooooooo=:
                            .+ooooooooooooo=ooooooooooooo+:
                          :=ooooooooooooo=+ +=ooooooooooooo+:
                        .+ooooooooooooo=+     +=ooooooooooooo+:
       + +    + +   + :=ooooooooooooo=:         ++ooooooooooooo+:
   :==++o+++++++++++==oooooooooooooo++++o+o+oo++++oo+oooooooooooo=:
  =ooooooooooooooooooooooooo+oooooooooooooooooooooooooooooooooooooo=+
 :oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo=
 +oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo:
  :=oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo+:
    +::==:=========.:.+oooooooooooooo===:=::::::=::::=::.:::=...::
                      +:oooooooooooooo:
                         =+ooooooooooooo=+
                           .+ooooooooooooo=+
                            +:+ooooooooooooo=+
     Powered By                :+ooooooooooooo=+
                                 .oooooooooooooo:
     mind+machine                 +.oooooooooooo+
                                     :+ooooooooo=
                                       :+ooooo+.
                                         +:::+
"""
import pandas as pd
import numpy as np
import json
import difflib

pseudonym_dict = {'gender': 'SEX',
                  'sex': 'SEX',
                  'race': 'RACE',
                  'ethnicity': 'RACE',
                  'age': 'AGE',
                  'height': 'HEIGHTBL',
                  'weight': 'WEIGHTBL',
                  'bmi': 'BMIBL'}  # Can be read from a flat file as well


def recommend_standard(x, ps_dict):
    pseudonym_vars = [x for x in ps_dict]
    try:
        return [ps_dict[x.lower()]]
    except:
        close_matches = difflib.get_close_matches(
            x.lower(), pseudonym_vars, n=3, cutoff=0.6)
        return [ps_dict[x] for x in close_matches]
