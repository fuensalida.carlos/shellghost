"""
                                        :==+==+
                                      :+ooooooo=:
                                    :=ooooooooooo=+
                                  :=ooooooooooooooo+:
                                .=ooooooooooooo+ooooo+:
                              :=ooooooooooooooooooooooo=:
                            .+ooooooooooooo=ooooooooooooo+:
                          :=ooooooooooooo=+ +=ooooooooooooo+:
                        .+ooooooooooooo=+     +=ooooooooooooo+:
       + +    + +   + :=ooooooooooooo=:         ++ooooooooooooo+:
   :==++o+++++++++++==oooooooooooooo++++o+o+oo++++oo+oooooooooooo=:
  =ooooooooooooooooooooooooo+oooooooooooooooooooooooooooooooooooooo=+
 :oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo=
 +oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo:
  :=oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo+:
    +::==:=========.:.+oooooooooooooo===:=::::::=::::=::.:::=...::
                      +:oooooooooooooo:
                         =+ooooooooooooo=+
                           .+ooooooooooooo=+
                            +:+ooooooooooooo=+
     Powered By                :+ooooooooooooo=+
                                 .oooooooooooooo:
     mind+machine                 +.oooooooooooo+
                                     :+ooooooooo=
                                       :+ooooo+.
                                         +:::+
"""
from flask import Flask, request, send_from_directory
from jinja2 import FileSystemLoader, Environment
import os
import json as json
import pandas as pd
import numpy as np
import urllib.parse
import werkzeug as we
from flask_cors import cross_origin
from flask import Flask, abort, render_template, request, make_response, jsonify
import harmonization_script
import recommendations
import difflib


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config.update(
    UPLOADED_PATH=os.path.join(basedir, 'uploads')
)

flag_data = 1


new_keys = []
@app.route('/recommendations/', methods=['POST', 'GET'])
@cross_origin()
def recommend():
    try:

        if request.method == 'POST':
            record = json.loads(request.data)
            old_keys = record['col_list']
            for old_key in old_keys:
                new_keys.append(recommendations.recommend_standard(
                    old_key, recommendations.pseudonym_dict))
            return jsonify(dict(zip(old_keys, new_keys)))

    except (we.exceptions.HTTPException, we.exceptions.BadRequestKeyError) as e:
        return jsonify(code=e.code, name=e.name, description=e.description)
    except NameError:
        return jsonify(code="806", name="NameError", description="NameError")
    except FileNotFoundError:
        return jsonify(code="807", name="FileNotFoundError", description="FileNotFoundError")
    except ValueError:
        return jsonify(code="808", name="ValueError", description="ValueError")
    except AttributeError:
        return jsonify(code="809", name="AttributeError", description="AttributeError")
    except Exception:
        return jsonify(code="500", name="ServerError", description="ServerError")
    return "EXIT RECOMMEND"
    # render_template('index.html', message="Back To Home")


@app.route('/', methods=['POST', 'GET'])
@cross_origin()
def upload():
    try:
        if request.method == 'POST':
            print(request.form.get('json'))
            jsonOutput = request.files['file']
            df = pd.read_csv(jsonOutput)
            harmonization_json = json.loads(request.form.get('json'))

            ''' harmonization can be customized in here '''
            harmonized_df = harmonization_script.harmonizer(
                df, harmonization_json)

            resp = make_response(harmonized_df.to_csv())
            resp.headers["Content-Disposition"] = "attachment; filename=export.csv"
            resp.headers["Content-Type"] = "text/csv"
            return resp

    except (we.exceptions.HTTPException, we.exceptions.BadRequestKeyError) as e:
        return jsonify(code=e.code, name=e.name, description=e.description)
    except NameError:
        return jsonify(code="806", name="NameError", description="NameError")
    except FileNotFoundError:
        return jsonify(code="807", name="FileNotFoundError", description="FileNotFoundError")
    except ValueError:
        return jsonify(code="808", name="ValueError", description="ValueError")
    except AttributeError:
        return jsonify(code="809", name="AttributeError", description="AttributeError")
    except Exception:
        return jsonify(code="500", name="ServerError", description="ServerError")
    return render_template('index.html', message="OK 200")


if __name__ == '__main__':
    app.run(debug=True)
