"""
                                        :==+==+
                                      :+ooooooo=:
                                    :=ooooooooooo=+
                                  :=ooooooooooooooo+:
                                .=ooooooooooooo+ooooo+:
                              :=ooooooooooooooooooooooo=:
                            .+ooooooooooooo=ooooooooooooo+:
                          :=ooooooooooooo=+ +=ooooooooooooo+:
                        .+ooooooooooooo=+     +=ooooooooooooo+:
       + +    + +   + :=ooooooooooooo=:         ++ooooooooooooo+:
   :==++o+++++++++++==oooooooooooooo++++o+o+oo++++oo+oooooooooooo=:
  =ooooooooooooooooooooooooo+oooooooooooooooooooooooooooooooooooooo=+
 :oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo=
 +oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo:
  :=oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo+:
    +::==:=========.:.+oooooooooooooo===:=::::::=::::=::.:::=...::
                      +:oooooooooooooo:
                         =+ooooooooooooo=+
                           .+ooooooooooooo=+
                            +:+ooooooooooooo=+
     Powered By                :+ooooooooooooo=+
                                 .oooooooooooooo:
     mind+machine                 +.oooooooooooo+
                                     :+ooooooooo=
                                       :+ooooo+.
                                         +:::+
"""
from flask import Flask, request, send_from_directory
from jinja2 import FileSystemLoader, Environment
import os
import json as json
import pandas as pd
import numpy as np
import urllib.parse
import werkzeug as we
from flask_cors import cross_origin
from flask import Flask, abort, render_template, request, make_response, jsonify


def anonymize(array):
    rand_letter = np.random.choice(
        ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'X', 'Y', 'Z'])
    rand_number = np.random.choice([10000, 100000, 1000000])
    new_array = [rand_number+i for i in range(len(array))]
    new_array = [rand_letter+str(i) for i in new_array]
    return (dict(zip(array, new_array)))


def harmonizer(df, h_json):

    harmonized_df = df.copy()
    # orig_cols = []
    other_fields = []

    for var in h_json:
        if var in ['Calculations', 'Conditionals']:
            other_fields.append(var)
            continue

        else:
            for harmodict in h_json[var]:
                if harmodict['Harmonize'] == 'N':
                    continue
                else:
                    if harmodict['type'] == 'controlled':
                        harmonized_df[harmodict['Standard']] = df[var].replace(
                            harmodict['additional_input']['map'])
                    elif harmodict['type'] == 'metric_based':
                        unit = harmodict['additional_input']['unit']
                        conversion_factor = harmodict['additional_input']['conversion_factor'][unit]
                        harmonized_df[harmodict['Standard']
                                      ] = df[var]*conversion_factor
                    elif harmodict['type'] == 'renamed':
                        harmonized_df[harmodict['Standard']] = df[var]
                    elif harmodict['type'] == 'Anonymized':
                        anon_dict = anonymize(
                            np.unique(harmonized_df[var]))
                        harmonized_df[var +
                                      '_anonymized'] = harmonized_df[var].replace(anon_dict)

    for f_type in other_fields:
        for vardict in h_json[f_type]:

            if f_type == 'Calculations':
                harmonized_df[vardict['name']] = harmonized_df.eval(
                    vardict['formula'])
            if f_type == 'Conditionals':
                conds = [harmonized_df.eval(x) for x in vardict['conditions']]
                outputs = [x for x in vardict['outputs']]
                harmonized_df[vardict['name']] = np.select(
                    conds, outputs, default=vardict['default'])

    return harmonized_df
