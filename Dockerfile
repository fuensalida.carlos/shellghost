FROM python:3.7.9-alpine3.12
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk update && apk add --no-cache curl vim wget bash gcc musl-dev linux-headers
RUN mkdir /app
WORKDIR /app
COPY . /app/
RUN pip install flask flask-cors pandas jinja2 numpy werkzeug
EXPOSE 5000
